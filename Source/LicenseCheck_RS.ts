/**
 * Module Description...
 *
 * @copyright 2019 Extend Apps.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType Restlet
 */

//noinspection ES6UnusedImports
import {EntryPoints} from 'N/types';
// @ts-ignore
import {ILicenseCheck, LicenseCheckResponse, LicenseManager} from '/SuiteApps/com.extendapps.licensemanager/LicenseManager';

// noinspection JSUnusedGlobalSymbols
export let get: EntryPoints.RESTlet.get = (context: ILicenseCheck) => {
    let licenseCheckResponse: LicenseCheckResponse = {
        isValid: false
    };
    licenseCheckResponse.isValid = LicenseManager.hasValidLicense(context);

    return licenseCheckResponse;
};