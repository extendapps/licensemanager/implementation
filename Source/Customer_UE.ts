/**
 * Module Description...
 *
 * @file License_UE.js
 * @copyright 2018 Extend Apps
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType UserEventScript
 */


//noinspection ES6UnusedImports
import {EntryPoints} from 'N/types';
import * as search from 'N/search';
// @ts-ignore
import {LicenseManager} from '/SuiteApps/com.extendapps.licensemanager/LicenseManager';

// noinspection JSUnusedGlobalSymbols
export let beforeLoad: EntryPoints.UserEvent.beforeLoad = (context: EntryPoints.UserEvent.beforeLoadContext) => {
    if (context.request && context.request.parameters && context.request.parameters['clearCache']) {
        search.create({
            type: 'customrecord_suiteapp_license',
            filters: [
                ['custrecord_license_customer', 'anyof', context.newRecord.id]
            ],
            columns: [
                'custrecord_license_customer',
                'custrecord_license_suiteapp'
            ]
        }).run().each(result => {
            LicenseManager.refreshLicenseStatus({
                accountId: <string>result.getValue(result.columns[0]),
                suiteAppId: +result.getValue(result.columns[1])
            });
            return true;
        });
    }
};