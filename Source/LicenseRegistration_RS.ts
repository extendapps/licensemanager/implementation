/**
 * Module Description...
 *
 * @copyright 2018 Extend Apps
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType Restlet
 */

//noinspection ES6UnusedImports
import {EntryPoints} from 'N/types';
// @ts-ignore
import {IRegistrationInfo, LicenseCheckResponse, LicenseManager} from '/SuiteApps/com.extendapps.licensemanager/LicenseManager';

// noinspection JSUnusedGlobalSymbols
export let post: EntryPoints.RESTlet.post = (context: IRegistrationInfo) => {
    let registrationResponse: LicenseCheckResponse = {
        isValid: false
    };
    registrationResponse.isValid = LicenseManager.registerLicense(context);

    return registrationResponse;
};