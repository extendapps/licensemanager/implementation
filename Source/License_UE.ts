/**
 * Module Description...
 *
 * @file License_UE.js
 * @copyright 2018 Extend Apps
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope SameAccount
 * @NScriptType UserEventScript
 */


//noinspection ES6UnusedImports
import {EntryPoints} from 'N/types';
// @ts-ignore
import {LicenseManager} from '/SuiteApps/com.extendapps.licensemanager/LicenseManager';
import * as record from 'N/record';
import * as config from 'N/config';
import * as email from 'N/email';

// noinspection JSUnusedGlobalSymbols
export let afterSubmit: EntryPoints.UserEvent.afterSubmit = (context: EntryPoints.UserEvent.afterSubmitContext) => {
    LicenseManager.refreshLicenseStatus({
        accountId: <string>context.newRecord.getValue({fieldId: 'custrecord_license_netsuite_acct'}),
        suiteAppId: +context.newRecord.getValue({fieldId: 'custrecord_license_suiteapp'})
    });

    if (context.type === context.UserEventType.CREATE) {
        let license: record.Record = record.load({
            type: context.newRecord.type,
            id: +context.newRecord.id
        });

        let customer: record.Record = record.load({
            type: record.Type.CUSTOMER,
            id: +license.getValue({fieldId: 'custrecord_license_customer'})
        });

        let installDetails = {
            companyname: customer.getValue({fieldId: 'companyname'}),
            url: customer.getValue({fieldId: 'url'}),
            accountId: license.getValue({fieldId: 'custrecord_license_netsuite_acct'}),
            email: customer.getValue({fieldId: 'email'}),
            suiteApp: license.getText({fieldId: 'custrecord_license_suiteapp'})
        };

        let companyInfo: record.Record = config.load({type: config.Type.COMPANY_INFORMATION});

        email.send({
            author: -5,
            body: JSON.stringify(installDetails),
            recipients: [<string>companyInfo.getValue({fieldId: 'email'})],
            subject: `New ${license.getText({fieldId: 'custrecord_license_suiteapp'})} Installation`
        });
    }
};